plowsof here. (yeah i had to make a sock account on gitlab to use its runners before i figure out how to set them up on our own instance)

These 2 files on the CCS Backend are responsible for parsing and displaying proposals on the Ideas or Funding Required pages.

- [ProcessProposals.php](https://repo.getmonero.org/monero-project/ccs-back/-/blob/master/app/Console/Commands/ProcessProposals.php?ref_type=heads)
- [UpdateSiteProposals.php](https://repo.getmonero.org/monero-project/ccs-back/-/blob/master/app/Console/Commands/UpdateSiteProposals.php?ref_type=heads)

This repo is the culmination of 2 days dealing with Gitlabs UI and its alien CI/CD pipelines with tears in my eyes and provides an example of running a workflow when someone makes a ccs merge request to show if it breaks the ideas/funding required parser. 

Description is compared (ignoring the front matter) to the file in the merge request and they must be identical. An improvement would be to run the pipeline when the description is changed but afaict this is not available out of the box. I tried merge event update webhook -> use the gitlab api to run the pipeline but the path i took didnt have the merge request description, it is possible but would also require an api key with expiry date / probably too many permissions. The maintainer can still run it manually before merging of course. 

Security concerns: people making a merge request to the ccs with a modified pipeline script **will** run arbrtrary code on our servers, by design, its how github workflows work so contributors can PR fixes. But the ccs gitlab is 'our' machine so i doubt we want to enable that. so i had to squint even harder to see through the tears but eventually a stackexchange answer revealed that we can pass a URL to the pipeline location and use an external one which can not be modified by merge requests. I pointed to one hosted on [my gist](https://gist.github.com/plowsof/be6d8cb587a1af1fb0e6f004c1708427) which can check the hashes of external scripts (controlled by the submitter) before running them.

at worst: i have learned 'why a proposal won't show on ideas/funding' and i can manually copy/paste peoples proposals here/check/advise.
at best: we have runners on our ccs gitlab and proposers can see exactly whats going wrong.